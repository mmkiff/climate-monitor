'use strict';

module.exports = function(app, temperatureController, weatherController) {
	app.route('/temperature')
		.get((req, res) => { temperatureController.getTemperature(req, res); }); // These can be chained together. Just add more below like this:
		//.post(controller.saveTemperature);
	
	app.route('/temperature/:temperature')
		.post((req, res) => { temperatureController.saveTemperature(req, res); });
		
	app.route('/temperatureHistory')
		.get((req, res) => { temperatureController.getTemperatureHistory(req, res); });
		
	app.route('/weather')
		.get((req, res) => { weatherController.getWeather(req, res); });
};