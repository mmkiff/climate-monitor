'use strict';
{
	var sqlite3 = require('sqlite3').verbose();
	
	// TO DO - move these to the model folder
	function CreateError(message) {
		return {
			success: false,
			message: message
		};
	}

	function CreateTemperatureObject(temperature, time) {
		return {
			temperature: temperature,
			timestamp: time
		};
	}


	class ClimateController {
		constructor(io) {
			this.io = io;
			this.db = new sqlite3.Database('database.sqlite');
			this.db.run("CREATE TABLE IF NOT EXISTS temperature (temperature TEXT, time INTEGER)");
			
			this.io.on('connection', (socket) => {
			  console.log('a user connected');
			  
			  this.currentTemperature((temperature) => {
				  io.emit('currentTemperature', temperature);
			  });
			});
		}
		
		currentTemperature(callback) {
			this.db.get("SELECT temperature, time FROM temperature ORDER BY time DESC LIMIT 1", function(err, row) {
				var result = null;
				if(err) {
					console.log(err.message);
					result = CreateError(err.message);
				}
				else
					result = CreateTemperatureObject(row.temperature, new Date(row.time));
				
				callback(result);
			});	
		}
		
		getTemperature(req, res) {
			this.currentTemperature((temperature) => {
				res.send(temperature);
			});
		}
		
		getTemperatureHistory(req, res) {
			this.db.all("SELECT temperature, time FROM temperature ORDER BY time DESC", (err, rows) => {
				var results = [];
				for(var i = 0; i < rows.length; i++) {
					var temp = CreateTemperatureObject(rows[i].temperature, new Date(rows[i].time));
					results.push(temp);
				}
				res.send(results);
			});	
		}
		
		saveTemperature(req, res) {
			var result = CreateTemperatureObject(req.params.temperature, new Date());
			this.db.run(`INSERT INTO temperature (temperature, time) VALUES(?,?)`, [result.temperature, result.timestamp], (err) => {
				if (err) {
					console.log(err.message);
					result = CreateError(err.message);
				}
				else {
					result.success = true;
					this.io.emit('currentTemperature', result);
				}
				
				res.send(result);
			});
		}
	}
	
	exports.ClimateController = ClimateController;
}




