'use strict';
{
	var axios = require('axios');
	
	class WeatherController {
		constructor(io) {
			this.apiKey = '4ebb4a992480510d';
			this.pws = 'ICALGARY23';
			this.apiUrl = `http://api.wunderground.com/api/${this.apiKey}/geolookup/conditions/q/pws:${this.pws}.json`;
									
			this.cloudyConditions = ['haze', 'overcast', 'partly cloudy', 'cloudy', 'mostly cloudy', 'mist', 'freezing fog'];
			this.sunnyConditions = ['clear', 'scattered clouds'];
			this.rainyConditions = ['light rain mist', 'thunderstorm', 'rain', 'rain mist', 'light showers rain'];
			this.snowyConditions = ['snow showers', 'snow', 'light snow mist', 'light snow'];
		}
		
		getWeather(req, res) {
			axios.get(this.apiUrl)
			.then(response => {
				response.data.current_observation.weather
				
				var epochTime = parseInt(response.data.current_observation.observation_epoch);
				var weather = {
					stationLocation: response.data.current_observation.observation_location.full,
					stationId: response.data.current_observation.station_id,
					lastUpdate: new Date(epochTime * 1000),
					conditions: response.data.current_observation.weather,
					temp_f: response.data.current_observation.temp_f,
					temp_c: response.data.current_observation.temp_c,
					feelsLike_f: response.data.current_observation.feelslike_f,
					feelsLike_c: response.data.current_observation.feelslike_c,
					url: response.data.current_observation.forecast_url,
					image: response.data.current_observation.icon_url
				};

				res.send(weather);
			})
			.catch(e => {
				console.log(e);
				res.send({
					success: false,
					details: e
				});
			});
		}
	}
	
	exports.WeatherController = WeatherController;
}




