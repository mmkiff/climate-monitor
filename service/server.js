var express = require('express'),
	app = express(),
	port = process.env.PORT || 3000,
	bodyParser = require('body-parser'),
	http = require('http').Server(app),
	io = require('socket.io')(http);
	
var ClimateController = require('./controllers/climateController');
var WeatherController = require('./controllers/weatherController');
  
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:8080");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Credentials", "true");
  
  next();
});

var routes = require('./routes/routes');
var temperatureController = new ClimateController.ClimateController(io); // TO DO - temp/climate naming
var weatherController = new WeatherController.WeatherController();
routes(app, temperatureController, weatherController);





http.listen(port);

console.log('Service started on: ' + port);