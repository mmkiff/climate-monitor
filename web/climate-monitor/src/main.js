// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';

import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
import VueSocketio from 'vue-socket.io';
import moment from 'moment';

Vue.use(VueSocketio, 'http://localhost:3000/');
Vue.use(VueMaterial);

Vue.config.productionTip = false;


Vue.filter('formatDate', function (value) {
	if (!value) 
		return ''
	return moment(String(value)).format('MMM D, YYYY @ h:mm a');
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
});
